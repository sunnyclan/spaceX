package com.example.spacexdemo.repository

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.spacexdemo.R
import com.example.spacexdemo.model.RocketListingResponse
import com.example.spacexdemo.network.AuthApi
import com.example.spacexdemo.network.RetrofitClient
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import com.example.spacexdemo.model.RocketDetailsResponse

class RocketRepository {
    val service= RetrofitClient.retrofit!!.create(AuthApi::class.java)
    val rocketListMutableLiveData=MutableLiveData<ArrayList<RocketListingResponse>?>()
    val rocketErrorMutableLiveData=MutableLiveData<String>()
    val rocketDetailsMutableLiveData=MutableLiveData<RocketDetailsResponse?>()
    val isLoading=MutableLiveData(false)

    fun getRocketListing(context: Context) {
        isLoading.postValue(true)
        service.getRocketList().enqueue(object :Callback<ArrayList<RocketListingResponse>>{
            override fun onResponse(
                call: Call<ArrayList<RocketListingResponse>>,
                response: Response<ArrayList<RocketListingResponse>>
            ) {
                 if (response.isSuccessful){
                     val res = response.body()
                     rocketListMutableLiveData.postValue(res)
                 }else if (response.code()==500){
                     rocketErrorMutableLiveData.postValue(context.getString(R.string.internal_server_error))
                 }else{
                     rocketErrorMutableLiveData.postValue(context.getString(R.string.unknown_error))
                 }
                isLoading.postValue(false)
            }

            override fun onFailure(call: Call<ArrayList<RocketListingResponse>>, t: Throwable) {
                rocketErrorMutableLiveData.postValue(context.getString(R.string.parsing_error))
                isLoading.postValue(false)
                Log.e("retrofit_fail", t.toString())
            }
        })
    }

    fun getRocketDetails(context: Context,url:String) {
        isLoading.postValue(true)
        service.getRocketDetails(url).enqueue(object :Callback<RocketDetailsResponse>{
            override fun onResponse(
                call: Call<RocketDetailsResponse>,
                response: Response<RocketDetailsResponse>
            ) {
                if (response.isSuccessful){
                    val res = response.body()
                    rocketDetailsMutableLiveData.postValue(res)
                }else if (response.code()==500){
                    rocketErrorMutableLiveData.postValue(context.getString(R.string.internal_server_error))
                }else{
                    rocketErrorMutableLiveData.postValue(context.getString(R.string.unknown_error))
                }
                isLoading.postValue(false)
            }

            override fun onFailure(call: Call<RocketDetailsResponse>, t: Throwable) {
                rocketErrorMutableLiveData.postValue(context.getString(R.string.parsing_error))
                isLoading.postValue(false)
                Log.e("retrofit_fail", t.toString())
            }

        })
    }

}