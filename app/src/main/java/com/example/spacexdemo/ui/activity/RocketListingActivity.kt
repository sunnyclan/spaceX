package com.example.spacexdemo.ui.activity

import android.app.NotificationManager
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.spacexdemo.R
import com.example.spacexdemo.databinding.ActivityRocketListingBinding
import com.example.spacexdemo.interfaces.OnClick
import com.example.spacexdemo.ui.adaptor.RocketListingAdaptor
import com.example.spacexdemo.viewmodel.RocketViewModel
import com.example.spacexdemo.viewmodel.RocketViewModelWithDagger
import androidx.activity.viewModels
import com.example.spacexdemo.db.table.OfflineRocketListTB
import com.example.spacexdemo.db.viewmodel.OfflineRocketListViewModel
import com.example.spacexdemo.di.repository.ResponseStatus
import com.example.spacexdemo.interfaces.NetworkConnectivityListener
import com.example.spacexdemo.model.RocketListingResponse
import com.example.spacexdemo.ui.adaptor.OfflineRocketListingAdaptor
import com.example.spacexdemo.utils.NetworkConnectivityManager
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RocketListingActivity : AppCompatActivity() ,OnClick,NetworkConnectivityListener{

    private lateinit var binding:ActivityRocketListingBinding
    private lateinit var viewModel:RocketViewModel
    private lateinit var rocketListingAdaptor: RocketListingAdaptor
    private val rocketViewModelWithDagger: RocketViewModelWithDagger by viewModels()
    private lateinit var offlineRocketListViewModel: OfflineRocketListViewModel
    private var offlineRocketList=emptyList<OfflineRocketListTB> ()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this@RocketListingActivity,R.layout.activity_rocket_listing)
        binding.executePendingBindings()

        //register offline viewModel
        offlineRocketListViewModel = OfflineRocketListViewModel(application)
        offlineRocketListViewModel = ViewModelProvider(this)[OfflineRocketListViewModel::class.java]
        offlineRocketListViewModel.offlineRocketList.observe(this){
            if (it.isNotEmpty()){
                offlineRocketList=it
            }else{
                Toast.makeText(this@RocketListingActivity,getString(R.string.saving_rockets_listing_details_offline),Toast.LENGTH_SHORT).show()
            }
        }

        viewRender()
//         apiCall()
        apiCallWithDagger()

    }

    private fun viewRender(){
        binding.header.titleTv.text= getString(R.string.rocket_listing)
        binding.listingRecyclerview.setHasFixedSize(false)
        binding.listingRecyclerview.layoutManager=LinearLayoutManager(this@RocketListingActivity)
        rocketListingAdaptor = RocketListingAdaptor(this@RocketListingActivity,this)
        //assign adaptor
        binding.listingRecyclerview.adapter=rocketListingAdaptor
    }

    private fun apiCall(){
        //register the viewModel
        viewModel= RocketViewModel(application)
        viewModel=ViewModelProvider(this)[RocketViewModel::class.java]
        viewModel.getRocketListing(this@RocketListingActivity)

        //making api call
        viewModel.getRocketListing(this)

        //handling response with observer
        viewModel.rocketListMutableLiveData.observe(this@RocketListingActivity){res->
            if (res!=null){
                //binding data
                bindDataToAdaptor(res)
            }
        }

        //handling error response
        viewModel.rocketErrorMutableLiveData.observe(this@RocketListingActivity){e->
            if (e!=null){
                Toast.makeText(this@RocketListingActivity,e,Toast.LENGTH_SHORT).show()
                binding.noDataTv.visibility=View.VISIBLE
                binding.listingRecyclerview.visibility=View.GONE
            }
        }

        //show/hide loading
        viewModel.isLoading.observe(this@RocketListingActivity){loading->
            if (loading){
                binding.spinKit.visibility=View.VISIBLE
            }else{
                binding.spinKit.visibility=View.GONE
            }
        }
    }

    private fun apiCallWithDagger(){

        rocketViewModelWithDagger.fetchRocketListFromServer()
        rocketViewModelWithDagger.rocketListResponse.observe(this){res->
            when(res.status){
                ResponseStatus.SUCCESS->{
                    binding.spinKit.visibility=View.GONE
                    res.data!!.rocketListingResponse?.let { bindDataToAdaptor(it) }
                 }

                 ResponseStatus.ERROR -> {
//                  handle error resp
                binding.spinKit.visibility=View.GONE
                binding.noDataTv.visibility=View.VISIBLE
                binding.listingRecyclerview.visibility=View.GONE
                  }

                ResponseStatus.LOADING->{
//                    showing loading
                    binding.spinKit.visibility=View.VISIBLE
                }
            }
        }
    }

    private fun bindDataToAdaptor(res:ArrayList<RocketListingResponse>){
        //handle success resp
        if (res.size>0){
            binding.noDataTv.visibility=View.GONE
            binding.listingRecyclerview.visibility=View.VISIBLE

            //updating data in adaptor
            rocketListingAdaptor.updateData(res)

            for ( i in 0 until res.size){
                for (j in offlineRocketList.indices){
                    if (res[i].id==offlineRocketList[j].id){
                        val data = OfflineRocketListTB(
                            id = res[i].id?:"",
                            img = res[i].flickrImages[0],
                            name = res[i].name?:"",
                            description = res[i].description?:"",
                            costPerLaunch = res[i].costPerLaunch.toString(),
                            successPercent = res[i].successRatePct.toString(),
                            height = res[i].secondStage?.payloads?.compositeFairing?.height?.meters.toString(),
                            diameter = res[i].diameter?.meters.toString(),
                            wikiLink = res[i].wikipedia.toString(),
                            status = res[i].active?:false,
                            country = res[i].country?:"",
                            engineCount = res[i].engines?.number.toString()
                        )
                        offlineRocketListViewModel.updateRocketList(data)
                    }
                }
            }

            // saving data offline
            for (i in 0 until res.size){
                offlineRocketListViewModel.isRecordExist(res[i].id?:""){
                   if (it){

                   }else{
                       val data = OfflineRocketListTB(
                           id = res[i].id?:"",
                           img = res[i].flickrImages[0],
                           name = res[i].name?:"",
                           description = res[i].description?:"",
                           costPerLaunch = res[i].costPerLaunch.toString(),
                           successPercent = res[i].successRatePct.toString(),
                           height = res[i].secondStage?.payloads?.compositeFairing?.height?.meters.toString(),
                           diameter = res[i].diameter?.meters.toString(),
                           wikiLink = res[i].wikipedia.toString(),
                           status = res[i].active?:false,
                           country = res[i].country?:"",
                           engineCount = res[i].engines?.number.toString()
                       )
                       offlineRocketListViewModel.insertRocketList(data)
                   }
                }
            }
        }else{
            binding.noDataTv.visibility=View.VISIBLE
            binding.listingRecyclerview.visibility=View.GONE
        }
    }

    override fun onClick(data: String) {

        val i =Intent(this@RocketListingActivity,RocketDetailsActivity::class.java)
        i.putExtra("rocket_id",data)
        startActivity(i)

    }

    override fun onResume() {
        super.onResume()
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancelAll()
        NetworkConnectivityManager.addListener(this)
        checkInternetConnectivity()
    }

    override fun onPause() {
        super.onPause()
        NetworkConnectivityManager.removeListener(this)
    }

    private fun checkInternetConnectivity() {
        this.let {
            NetworkConnectivityManager.notifyListeners(it)
        }
    }


    override fun onNetworkConnectivityChanged(isConnected: Boolean) {
        if (isConnected){

        }else{
            Toast.makeText(this,"you are serving offline",Toast.LENGTH_SHORT).show()
            offlineRocketListViewModel = OfflineRocketListViewModel(application)
            offlineRocketListViewModel = ViewModelProvider(this)[OfflineRocketListViewModel::class.java]
            offlineRocketListViewModel.offlineRocketList.observe(this){
                if (it.isNotEmpty()) {
                    binding.listingRecyclerview.setHasFixedSize(false)
                    binding.listingRecyclerview.layoutManager=LinearLayoutManager(this@RocketListingActivity)
                    val  offlineRocketListingAdaptor = OfflineRocketListingAdaptor(this@RocketListingActivity,this)
                    //assign adaptor
                    binding.listingRecyclerview.adapter=offlineRocketListingAdaptor
                    offlineRocketListingAdaptor.updateData(it)
                    binding.noDataTv.visibility=View.GONE
                    binding.listingRecyclerview.visibility=View.VISIBLE
                }
            }
        }
    }

}