package com.example.spacexdemo.ui.adaptor

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.spacexdemo.R
import com.example.spacexdemo.databinding.RocketListingItemBinding
import com.example.spacexdemo.interfaces.OnClick
import com.example.spacexdemo.model.RocketListingResponse


class RocketListingAdaptor(
    private var context: Context,
    var onClick: OnClick,
): RecyclerView.Adapter<RocketListingAdaptor.ViewHolder>() {

    private var rocketList=ArrayList<RocketListingResponse>()

    class ViewHolder(var binding: RocketListingItemBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val l= LayoutInflater.from(context)
        val binding= RocketListingItemBinding.inflate(l,parent,false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return  rocketList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (rocketList.size>0){
            val rocket=rocketList[position]

            //rendering images
            if (rocket.flickrImages.size>0){
                Glide.with(context)
                    .load(rocket.flickrImages[0])
                    .placeholder(R.drawable.splash_screen_logo)
                    .error(R.drawable.splash_screen_logo)
                    .apply(RequestOptions().centerCrop())
                    .into(holder.binding.imgIv)
            }
            // rocker info
            holder.binding.nameTv.text=rocket.name?:""
            holder.binding.countryNameTv.text=rocket.country?:""
            holder.binding.engineCountTv.text=rocket.engines?.number.toString()

            holder.itemView.setOnClickListener {
                onClick.onClick(rocket.id?:"")
            }
        }
    }

    fun updateData(res: ArrayList<RocketListingResponse>) {
        rocketList=res
        notifyDataSetChanged()
    }
}