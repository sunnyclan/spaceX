package com.example.spacexdemo.ui.activity

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.spacexdemo.R
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@SuppressLint("CustomSplashScreen")

class SplashScreenActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        startMainActivity{
            startActivity(Intent(this@SplashScreenActivity, RocketListingActivity::class.java))
            finish()
        }

    }

    @OptIn(DelicateCoroutinesApi::class)
    private fun startMainActivity(action: () -> Unit) {
        GlobalScope.launch(Dispatchers.Main) {
            delay(2000)
            action()
        }
    }
}