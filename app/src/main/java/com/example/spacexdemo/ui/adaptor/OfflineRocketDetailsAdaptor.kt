package com.example.spacexdemo.ui.adaptor

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.spacexdemo.R
import com.example.spacexdemo.databinding.RocketDetailsImagesItemBinding

class OfflineRocketDetailsAdaptor(
    private var context: Context
): RecyclerView.Adapter<OfflineRocketDetailsAdaptor.ViewHolder>() {

    private var flickerUrl=""

    class ViewHolder(var binding: RocketDetailsImagesItemBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val l= LayoutInflater.from(context)
        val binding= RocketDetailsImagesItemBinding.inflate(l,parent,false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return 1
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        //rendering flicker images
        Glide.with(context)
            .load(flickerUrl)
            .placeholder(R.drawable.splash_screen_logo)
            .error(R.drawable.splash_screen_logo)
            .apply(RequestOptions().centerCrop())
            .into(holder.binding.imgIv)

    }

    fun updateData(res:String) {
        flickerUrl=res
        notifyDataSetChanged()
    }

}