package com.example.spacexdemo.ui.activity

import android.annotation.SuppressLint
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.graphics.Rect
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.spacexdemo.R
import com.example.spacexdemo.databinding.ActivityRocketDetailsBinding
import com.example.spacexdemo.db.viewmodel.OfflineRocketListViewModel
import com.example.spacexdemo.di.repository.ResponseStatus
import com.example.spacexdemo.interfaces.NetworkConnectivityListener
import com.example.spacexdemo.model.RocketDetailsResponse
import com.example.spacexdemo.network.RetrofitClient
import com.example.spacexdemo.ui.adaptor.OfflineRocketDetailsAdaptor
import com.example.spacexdemo.ui.adaptor.RocketDetailsImagesAdaptor
import com.example.spacexdemo.utils.MyLayoutManager
import com.example.spacexdemo.utils.NetworkConnectivityManager
import com.example.spacexdemo.viewmodel.RocketDetailsViewModelWithDagger
import com.example.spacexdemo.viewmodel.RocketViewModel
import com.example.spacexdemo.viewmodel.RocketViewModelWithDagger
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RocketDetailsActivity : AppCompatActivity(),NetworkConnectivityListener {

    private lateinit var binding:ActivityRocketDetailsBinding
    private lateinit var viewModel: RocketViewModel
    private lateinit var rocketDetailsImagesAdaptor:RocketDetailsImagesAdaptor
    private var rocketid=""
    private val rocketViewModelWithDagger: RocketViewModelWithDagger by viewModels()
    private lateinit var offlineRocketListViewModel: OfflineRocketListViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_rocket_details)
        binding.executePendingBindings()

        rocketid = intent.getStringExtra("rocket_id")?:""

        binding.mainConst.visibility=View.GONE
        binding.header.backIv.visibility=View.VISIBLE
        binding.header.backIv.setOnClickListener { finish() }

        viewRender()
//       apiCall()
         apiCallWithDagger()

    }
    private fun viewRender(){
        binding.listingRecyclerview.setHasFixedSize(false)
        binding.listingRecyclerview.layoutManager= MyLayoutManager(this@RocketDetailsActivity,LinearLayoutManager.HORIZONTAL,false)
        rocketDetailsImagesAdaptor = RocketDetailsImagesAdaptor(this@RocketDetailsActivity)
        //assign adaptor
        binding.listingRecyclerview.adapter=rocketDetailsImagesAdaptor
    }

    @SuppressLint("StringFormatInvalid", "SetTextI18n")
    private fun apiCall(){
        //register the viewModel
        viewModel= RocketViewModel(application)
        viewModel= ViewModelProvider(this)[RocketViewModel::class.java]
        viewModel.getRocketListing(this@RocketDetailsActivity)

        //making api call
        viewModel.getRocketDetails(this,RetrofitClient.suburls.endPoint+"/"+rocketid)

        //handling response with observer
        viewModel.rocketDetailsMutableLiveData.observe(this@RocketDetailsActivity){res->
            if (res!=null){
                //calling fun for bind data
                 apiSuccessViewBinding(res)
            }
        }

        //handling error response
        viewModel.rocketErrorMutableLiveData.observe(this@RocketDetailsActivity){e->
            if (e!=null){
                Toast.makeText(this@RocketDetailsActivity,e, Toast.LENGTH_SHORT).show()
                binding.noDataTv.visibility=View.VISIBLE
                binding.listingRecyclerview.visibility=View.GONE
            }
        }

        //show/hide loading
        viewModel.isLoading.observe(this@RocketDetailsActivity){loading->
            if (loading){
                binding.spinKit.visibility=View.VISIBLE
            }else{
                binding.spinKit.visibility=View.GONE
            }
        }
    }


    private fun apiCallWithDagger(){
        rocketViewModelWithDagger.getRocketDetails(myUrl = RetrofitClient.suburls.endPoint+"/"+rocketid)
        rocketViewModelWithDagger.rocketDetailsResponse.observe(this){res->
            when(res.status) {
                ResponseStatus.SUCCESS->{
                    binding.spinKit.visibility=View.GONE
                    res.data!!.rocketDetailsResponse?.let { apiSuccessViewBinding(it) }
                }
                ResponseStatus.ERROR->{
                    binding.spinKit.visibility=View.GONE
                    binding.noDataTv.visibility=View.VISIBLE
                }
                ResponseStatus.LOADING->{
                    binding.spinKit.visibility=View.VISIBLE
                }
              }
            }
    }

    private fun openLink(url:String){
        try {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            val chooserIntent = Intent.createChooser(intent, "Choose Browser")
            this@RocketDetailsActivity.startActivity(chooserIntent)
        }catch (e:Exception){
            Toast.makeText(this@RocketDetailsActivity,getString(R.string.browser_not_found_on_this_device),Toast.LENGTH_SHORT).show()
        }
    }

    private fun apiSuccessViewBinding(res:RocketDetailsResponse){
        //handle empty data res
        if (res.flickrImages.size>0){
            binding.noDataTv.visibility=View.GONE
            binding.listingRecyclerview.visibility=View.VISIBLE

            //updating data in adaptor
            rocketDetailsImagesAdaptor.updateData(res.flickrImages)
        }

        // data binding to views
        binding.header.titleTv.text= res.name?:""
        binding.descriptionTv.text= res.description?:""
        if (res.active==true){
            binding.activeStatusTv.text= getString(R.string.active)
            binding.activeStatusTv.setTextColor(getColor(R.color.green))
        }else{
            binding.activeStatusTv.text= getString(R.string.in_active)
            binding.activeStatusTv.setTextColor(getColor(R.color.secondary))
        }
        binding.costTv.text= getString(R.string.doller_sign)+ (res.costPerLaunch ?: 0).toString()
        binding.successRateTv.text= (res.successRatePct ?: 0).toString() +getString(R.string.percent_sign)
        binding.heightTv.text=  (res.height?.meters ?: 0.0).toString()+" "+getString(R.string.meter)
        binding.diameterTv.text= (res.diameter?.meters ?: 0.0).toString()+" "+getString(R.string.meter)
        binding.wikiLinkTv.text= res.wikipedia?:""
        binding.mainConst.visibility=View.VISIBLE

        binding.wikiLinkTv.setOnClickListener { openLink(res.wikipedia?:"") }
    }
    override fun onResume() {
        super.onResume()
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancelAll()
        NetworkConnectivityManager.addListener(this)
        checkInternetConnectivity()
    }

    override fun onPause() {
        super.onPause()
        NetworkConnectivityManager.removeListener(this)
    }

    private fun checkInternetConnectivity() {
        this.let {
            NetworkConnectivityManager.notifyListeners(it)
        }
    }

    override fun onNetworkConnectivityChanged(isConnected: Boolean) {
         if (!isConnected){
             rocketid = intent.getStringExtra("rocket_id")?:""
             offlineRocketListViewModel = OfflineRocketListViewModel(application)
             offlineRocketListViewModel = ViewModelProvider(this)[OfflineRocketListViewModel::class.java]
             offlineRocketListViewModel.offlineRocketList.observe(this){
                 if (it.isNotEmpty()) {
                     for (i in it.indices){
                         if (rocketid==it[i].id){
                             binding.header.titleTv.text=it[i].name
                             binding.costTv.text= getString(R.string.doller_sign)+ it[i].costPerLaunch
                             binding.successRateTv.text= it[i].successPercent +getString(R.string.percent_sign)
                             if (it[i].height!="null"){
                                 binding.heightTv.text= it[i].height  +" "+getString(R.string.meter)
                             }else{
                                 binding.heightTv.text= "0 "+getString(R.string.meter)
                             }
                             binding.diameterTv.text= it[i].diameter+" "+getString(R.string.meter)
                             binding.wikiLinkTv.text=it[i].wikiLink
                             binding.descriptionTv.text=  it[i].description?:""
                             if (it[i].status==true){
                                 binding.activeStatusTv.text= getString(R.string.active)
                                 binding.activeStatusTv.setTextColor(getColor(R.color.green))
                             }else{
                                 binding.activeStatusTv.text= getString(R.string.in_active)
                                 binding.activeStatusTv.setTextColor(getColor(R.color.secondary))
                             }
                             binding.mainConst.visibility=View.VISIBLE
                             binding.noDataTv.visibility=View.GONE
                             binding.listingRecyclerview.setHasFixedSize(false)
                             binding.listingRecyclerview.layoutManager= LinearLayoutManager(this@RocketDetailsActivity,LinearLayoutManager.HORIZONTAL,false)
                             val  offlineRocketDetailsAdaptor = OfflineRocketDetailsAdaptor(this@RocketDetailsActivity)
                             //assign adaptor
                             binding.listingRecyclerview.adapter=offlineRocketDetailsAdaptor
                             offlineRocketDetailsAdaptor.updateData(it[i].img)
                         }
                     }
                 }
             }
         }
    }
}