package com.example.spacexdemo.utils

import android.content.Context
import com.example.spacexdemo.interfaces.NetworkConnectivityListener

object NetworkConnectivityManager {
    private val listeners = mutableListOf<NetworkConnectivityListener>()

    fun addListener(listener: NetworkConnectivityListener) {
        listeners.add(listener)
    }

    fun removeListener(listener: NetworkConnectivityListener) {
        listeners.remove(listener)
    }

    fun notifyListeners(context: Context) {
        val isConnected = NetworkUtils.isInternetConnected(context)
        for (listener in listeners) {
            listener.onNetworkConnectivityChanged(isConnected)
        }
    }
}
