package com.example.spacexdemo.utils

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MyLayoutManager(context: Context, orientation: Int, reverseLayout: Boolean) : LinearLayoutManager(context, orientation, reverseLayout) {

        override fun checkLayoutParams(lp: RecyclerView.LayoutParams): Boolean {
            lp.width = 85.percentOf(width) // Adjust percentage as needed
            return super.checkLayoutParams(lp)
        }

        private fun Int.percentOf(value: Int): Int {
            return if (this == 0) 0 else ((this.toDouble() / 100) * value).toInt()
        }
    }