package com.example.spacexdemo.di.repository

import com.example.spacexdemo.di.auth.AuthApi
import com.example.spacexdemo.model.RocketDetailsResponse
import com.example.spacexdemo.model.RocketListingResponse
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GeneralRepository @Inject constructor(private val apiService: AuthApi) : GeneralDataSource {

    override suspend fun getRocketList(): Flow<ArrayList<RocketListingResponse>> {
        return flow { emit(apiService.getRocketList()) }
    }

    override suspend fun getRocketDetails(url: String): Flow<RocketDetailsResponse> {
        return flow { emit(apiService.getRocketDetail(url)) }
    }

}
