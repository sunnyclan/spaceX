package com.example.spacexdemo.di.repository

import com.example.spacexdemo.model.RocketDetailsResponse
import com.example.spacexdemo.model.RocketListingResponse
import kotlinx.coroutines.flow.Flow
import retrofit2.http.Url

interface GeneralDataSource {

    suspend fun getRocketList(): Flow<ArrayList<RocketListingResponse>>

    suspend fun getRocketDetails(url:String): Flow<RocketDetailsResponse>

}