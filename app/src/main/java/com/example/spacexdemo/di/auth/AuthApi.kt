package com.example.spacexdemo.di.auth


import com.example.spacexdemo.model.RocketDetailsResponse
import com.example.spacexdemo.model.RocketListingResponse
import retrofit2.http.GET
import retrofit2.http.Url

interface AuthApi {

    @GET(ApiRoutes.END_POINT)
    suspend fun getRocketList(): ArrayList<RocketListingResponse>

    @GET
    suspend fun getRocketDetail(@Url url: String): RocketDetailsResponse
}