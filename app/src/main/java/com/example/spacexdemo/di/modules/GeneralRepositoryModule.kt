package com.example.spacexdemo.di.modules

import com.example.spacexdemo.di.auth.AuthApi
import com.example.spacexdemo.di.repository.GeneralDataSource
import com.example.spacexdemo.di.repository.GeneralRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class GeneralRepositoryModule {

    @Singleton
    @Provides
    fun provideGeneralDataSource(authApi: AuthApi): GeneralDataSource {
        return GeneralRepository(authApi)
    }
}