package com.example.spacexdemo.di.modules


import com.example.spacexdemo.utils.DefaultDispatcherProvider
import com.example.spacexdemo.utils.DispatcherProvider
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object IDispatcherModule {

    @Singleton
    @Provides
    fun provideDispatcher(): DispatcherProvider = DefaultDispatcherProvider()
}