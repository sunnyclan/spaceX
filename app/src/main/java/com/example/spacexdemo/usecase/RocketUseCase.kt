package com.example.spacexdemo.usecase


import com.example.spacexdemo.di.repository.GeneralDataSource
import com.example.spacexdemo.model.RocketDetailsResponse
import com.example.spacexdemo.model.RocketListingResponse
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class RocketUseCase @Inject constructor(private val repository: GeneralDataSource) {
     suspend fun rocketListUseCase(): Flow<ArrayList<RocketListingResponse>> {
        return repository.getRocketList()
    }
    suspend fun rocketDetailUseCase(url:String): Flow<RocketDetailsResponse> {
        return repository.getRocketDetails(url)
    }
}