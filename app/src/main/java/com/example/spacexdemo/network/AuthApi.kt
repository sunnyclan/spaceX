package com.example.spacexdemo.network

import com.example.spacexdemo.model.RocketDetailsResponse
import com.example.spacexdemo.model.RocketListingResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Query
import retrofit2.http.Url

interface AuthApi {
    @Headers("Content-Type:application/json", "Accept:application/json")
    @GET(RetrofitClient.suburls.endPoint)
    fun getRocketList(): Call<ArrayList<RocketListingResponse>>

    @Headers("Content-Type:application/json", "Accept:application/json")
    @GET
    fun getRocketDetails(@Url url:String): Call<RocketDetailsResponse>
}