package com.example.spacexdemo.network

import com.example.spacexdemo.utils.CONSTANTS.BASE_URL
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitClient {
    @JvmStatic
    var retrofit: Retrofit? = null

        get() {
            val logLevel = HttpLoggingInterceptor()
            logLevel.setLevel(HttpLoggingInterceptor.Level.BODY)
            val httpClient = OkHttpClient.Builder()
            httpClient.addInterceptor(logLevel)
            httpClient.connectTimeout(120, TimeUnit.SECONDS)
            httpClient.readTimeout(120, TimeUnit.SECONDS)
            httpClient.writeTimeout(120, TimeUnit.SECONDS)
            if (field == null) {
                field = Retrofit.Builder().baseUrl(base_url)
                    .addConverterFactory(GsonConverterFactory.create()).client(httpClient.build())
                    .build()
            }
            return field
        }

    private const val base_url = BASE_URL
    object suburls {
        const val endPoint = "v4/rockets"
    }
}