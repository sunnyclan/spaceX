package com.example.spacexdemo.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.spacexdemo.di.repository.ResponseStatusCallbacks
import com.example.spacexdemo.model.RocketListDataModel
import com.example.spacexdemo.model.RocketListingResponse
import com.example.spacexdemo.usecase.RocketUseCase
import com.wateringwhole.user.utlis.DefaultStringResourceManager
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import androidx.lifecycle.viewModelScope
import com.example.spacexdemo.model.RocketDetailsDataModel
import com.example.spacexdemo.model.RocketDetailsResponse
import kotlinx.coroutines.launch


@HiltViewModel
class RocketViewModelWithDagger @Inject constructor(private val rocketUseCase: RocketUseCase) : ViewModel() {

    // getting rocket listing
    private val _rocketList = MutableLiveData<ResponseStatusCallbacks<RocketListDataModel>>()
    val rocketListResponse: LiveData<ResponseStatusCallbacks<RocketListDataModel>>
        get() = _rocketList

      fun fetchRocketListFromServer() {
        _rocketList.value = ResponseStatusCallbacks.loading(data = RocketListDataModel(ArrayList()))
        viewModelScope.launch {
            try {
                    rocketUseCase.rocketListUseCase().collect { dataset ->
                        dataset.let {
                            if (it.size>0) {
                                _rocketList.postValue(
                                    ResponseStatusCallbacks.success(
                                        data = RocketListDataModel(dataset),  "unknown error"
                                    )
                                )
                            } else {
                                _rocketList.value = ResponseStatusCallbacks.error(
                                    data = RocketListDataModel(arrayListOf()),  "unknown error"
                                )
                            }
                        }
                    }

            } catch (e: Exception) {
                _rocketList.value = ResponseStatusCallbacks.error(null, e.message.toString())
            }
        }
    }

    // getting rocket details
    private var url:String=""
    private val _rocketDetails = MutableLiveData<ResponseStatusCallbacks<RocketDetailsDataModel>>()
    val rocketDetailsResponse: LiveData<ResponseStatusCallbacks<RocketDetailsDataModel>>
        get() = _rocketDetails

    private fun fetchRocketDetailsFromServer() {
        _rocketDetails.value = ResponseStatusCallbacks.loading(data = RocketDetailsDataModel(null))
        viewModelScope.launch {
            try {
                rocketUseCase.rocketDetailUseCase(url).collect { dataset ->
                    dataset.let {
                        if (it.id!=null) {
                            _rocketDetails.postValue(
                                ResponseStatusCallbacks.success(
                                    data = RocketDetailsDataModel(dataset),  "unknown error"
                                )
                            )
                        } else {
                            _rocketDetails.value = ResponseStatusCallbacks.error(
                                data = RocketDetailsDataModel(null),  "unknown error"
                            )
                        }
                    }
                }

            } catch (e: Exception) {
                _rocketDetails.value = ResponseStatusCallbacks.error(null, e.message.toString())
            }
        }
    }

    fun getRocketDetails(myUrl:String){
        url=myUrl
        fetchRocketDetailsFromServer()
    }

}