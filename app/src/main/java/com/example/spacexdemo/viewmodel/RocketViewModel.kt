package com.example.spacexdemo.viewmodel

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.spacexdemo.model.RocketDetailsResponse
import com.example.spacexdemo.model.RocketListingResponse
import com.example.spacexdemo.repository.RocketRepository

class RocketViewModel (application: Application) : AndroidViewModel(application) {

    private val repository = RocketRepository()

    fun getRocketListing(context: Context) {
        repository.getRocketListing(context)
    }

    fun getRocketDetails(context: Context,url:String) {
        repository.getRocketDetails(context,url)
    }

    val rocketListMutableLiveData:MutableLiveData<ArrayList<RocketListingResponse>?>
        get() = repository.rocketListMutableLiveData

    val rocketErrorMutableLiveData: MutableLiveData<String>
        get() = repository.rocketErrorMutableLiveData

    val rocketDetailsMutableLiveData:MutableLiveData<RocketDetailsResponse?>
        get() = repository.rocketDetailsMutableLiveData

    val isLoading:MutableLiveData<Boolean>
        get() = repository.isLoading

}