package com.example.spacexdemo.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.spacexdemo.di.repository.ResponseStatusCallbacks
import com.example.spacexdemo.model.RocketDetailsDataModel
import com.example.spacexdemo.usecase.RocketUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RocketDetailsViewModelWithDagger @Inject constructor(private val rocketUseCase: RocketUseCase) : ViewModel() {
    // getting rocket details
    private var url:String=""
    private val _rocketDetails = MutableLiveData<ResponseStatusCallbacks<RocketDetailsDataModel>>()
    val rocketDetailsResponse: LiveData<ResponseStatusCallbacks<RocketDetailsDataModel>>
        get() = _rocketDetails

    private fun fetchRocketDetailsFromServer() {
        _rocketDetails.value = ResponseStatusCallbacks.loading(data = RocketDetailsDataModel(null))
        viewModelScope.launch {
            try {
                rocketUseCase.rocketDetailUseCase(url).collect { dataset ->
                    dataset.let {
                        if (it.id!=null) {
                            _rocketDetails.postValue(
                                ResponseStatusCallbacks.success(
                                    data = RocketDetailsDataModel(dataset),  "unknown error"
                                )
                            )
                        } else {
                            _rocketDetails.value = ResponseStatusCallbacks.error(
                                data = RocketDetailsDataModel(null),  "unknown error"
                            )
                        }
                    }
                }

            } catch (e: Exception) {
                _rocketDetails.value = ResponseStatusCallbacks.error(null, e.message.toString())
            }
        }
    }

    fun getRocketDetails(myUrl:String){
        url=myUrl
        fetchRocketDetailsFromServer()
    }
}
