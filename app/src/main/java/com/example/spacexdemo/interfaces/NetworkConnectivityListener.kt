package com.example.spacexdemo.interfaces

interface NetworkConnectivityListener {
    fun onNetworkConnectivityChanged(isConnected: Boolean)
}