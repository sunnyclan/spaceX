package com.example.spacexdemo.db.roomsetup

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.spacexdemo.db.dao.OfflineRocketListDao
import com.example.spacexdemo.db.table.OfflineRocketListTB

@Database(entities = [ OfflineRocketListTB::class ], version = 1, exportSchema = false)

abstract class AppDatabase:RoomDatabase() {

  abstract fun offlineRocketListDao(): OfflineRocketListDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "SpaceXDb"
                )
                .build()
                INSTANCE = instance
                instance
            }
        }
    }
}