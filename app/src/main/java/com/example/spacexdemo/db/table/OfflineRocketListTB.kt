package com.example.spacexdemo.db.table

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "OfflineRocketListTB")

data class OfflineRocketListTB (
    @PrimaryKey(autoGenerate = true ) var srid : Long = 0,
    var id:String,
    var img:String,
    var name:String,
    var description:String,
    var costPerLaunch:String,
    var successPercent:String,
    var height:String,
    var diameter:String,
    var wikiLink:String,
    var status:Boolean,
    var country:String,
    var engineCount:String,
)