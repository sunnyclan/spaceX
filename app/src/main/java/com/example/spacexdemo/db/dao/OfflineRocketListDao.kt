package com.example.spacexdemo.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.spacexdemo.db.table.OfflineRocketListTB

@Dao
interface OfflineRocketListDao {

    @Insert
    suspend fun insert( offlineRocketListTB: OfflineRocketListTB)

    @Query("SELECT COUNT(*) FROM offlineRocketListTB WHERE id=:id")
    suspend fun isRecordExist(id:String):Int

    @Query("UPDATE offlineRocketListTB SET id=:id,img=:img,name=:name,description=:description,costPerLaunch=:costPerLaunch,successPercent=:successPercent,height=:height,diameter=:diameter,wikiLink=:wikiLink,status=:status,country=:country,engineCount=:engineCount WHERE id = :id")
    suspend fun update(     id:String,
                            img:String,
                            name:String,
                            description:String,
                            costPerLaunch:String,
                            successPercent:String,
                            height:String,
                            diameter:String,
                            wikiLink:String,
                            status:Boolean,
                            country:String,
                            engineCount:String)

    @Query("SELECT * FROM OfflineRocketListTB")
    fun  getOfflineRocketList(): LiveData<List<OfflineRocketListTB>>

}