package com.example.spacexdemo.db.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.spacexdemo.db.dao.OfflineRocketListDao
import com.example.spacexdemo.db.roomsetup.AppDatabase
import com.example.spacexdemo.db.table.OfflineRocketListTB
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch

class OfflineRocketListViewModel(application: Application) :AndroidViewModel(application){

    private val coroutineScope = CoroutineScope(Dispatchers.Main)
    private val offlineRocketListDao: OfflineRocketListDao
    val offlineRocketList: LiveData<List<OfflineRocketListTB>>

    init {
        offlineRocketListDao= AppDatabase.getInstance(application.applicationContext).offlineRocketListDao()
        offlineRocketList=offlineRocketListDao.getOfflineRocketList()
    }

     fun insertRocketList(offlineRocketListTB: OfflineRocketListTB){
        coroutineScope.launch {
            offlineRocketListDao.insert(offlineRocketListTB)
        }
    }

    fun updateRocketList(o: OfflineRocketListTB){
        coroutineScope.launch {
            offlineRocketListDao.update(o.id,o.img,o.name,o.description,o.costPerLaunch,o.successPercent,o.height,o.diameter,o.wikiLink,o.status,o.country,o.engineCount)
        }
    }

    fun isRecordExist( id:String ,onRowExists: (Boolean) -> Unit){
        coroutineScope.launch(Dispatchers.IO) {
            val c=offlineRocketListDao.isRecordExist(id)
            onRowExists(c>0)
        }
    }

}